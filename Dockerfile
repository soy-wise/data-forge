FROM python:3.12.0-bookworm

RUN pip install poetry==1.7.1

# set poetry to not create virtualenvs
ENV POETRY_VIRTUALENVS_CREATE=false
ENV POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /app

# copy and install dependencies
COPY pyproject.toml poetry.* ./
RUN touch README.md
RUN poetry install --without dev --no-root && rm -rf $POETRY_CACHE_DIR

COPY ./src ./src

EXPOSE 8080