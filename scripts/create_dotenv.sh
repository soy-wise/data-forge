##!/bin/bash

touch .env

while IFS= read line
do

    if [ -n "$line" ]; then
        IFS='=' read -ra env_var <<< "$line"

        name_ref="${env_var}_${BRANCH_TAG}"

        env_var_value=$(eval "echo \"\$$name_ref\"")

        if [ -z "$env_var_value" ]; then
            echo "${name_ref} is empty"
            exit 1
        fi
        
        echo "${env_var[0]}=${env_var_value}" >> .env
    fi

done <".env.example"
