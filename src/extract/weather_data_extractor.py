from src.utils.seed_places_parser import SeedPlacesParser

from meteostat import Stations, Daily
from datetime import datetime
from decouple import config
import pandas as pd

class WeatherDataExtractor:

    def __init__(self):

        SEED_SCHEMA = config('SEED_SCHEMA')
        self.seed_parser = SeedPlacesParser(SEED_SCHEMA)
        

    def __get_closest_station(
            self, place: dict, 
            stations_count: int = 5
    ) -> pd.DataFrame:

        stations = Stations()
        stations = stations.nearby(place['lat'], place['lon'])

        return stations.fetch(stations_count)
    
    def get_place_weather_history(
            self, place: dict, 
            start_date: datetime, 
            end_date: datetime
    ) -> pd.DataFrame:
        
        stations = self.__get_closest_station(place)
        weather_data = Daily(stations, start_date, end_date)

        return weather_data.fetch()
        
        
    def get_weather(self, ticker_name: str) -> dict[str, pd.DataFrame]:

        places = self.seed_parser.get_places(ticker_name)

        start_date = self.seed_parser.get_start_date(ticker_name)
        end_date = datetime.now()

        weather = {}

        for place in places:

            weather_data = self.get_place_weather_history(
                place, start_date, end_date
                )
            
            weather[place['city']] = weather_data

        return weather
