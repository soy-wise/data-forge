import yfinance as yf
import pandas as pd

class FinanceDataExtractor:

    def __init__(self, ticker_name: str) -> None:
        self.ticker_name = ticker_name
        self.ticker_object = yf.Ticker(self.ticker_name)

    def get_ticker_info(self) -> dict:
        return self.ticker_object.info
    
    def get_ticker_data(self) -> pd.DataFrame:
        return yf.download(self.ticker_name)