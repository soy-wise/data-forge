
class ETLProcessError(Exception):
    """Base custom exception for ETL process errors."""
    pass


class InvalidDateError(ETLProcessError):
    """Exception raised when an invalid date is passed to the ETL process.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)