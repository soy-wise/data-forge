from datetime import datetime
import json

class SeedPlacesParser:
    def __init__(self, path):
        self.path = path
        self.places = self.parse()

    def parse(self):
        with open(self.path) as f:
            data = json.load(f)
        return data

    def get_places(self, ticker_name):
        return self.places[ticker_name]['places']
    
    def get_start_date(self, ticker_name):
        start_date = self.places[ticker_name]['start_date']

        return datetime.fromisoformat(start_date)
    
    def get_schema(self, ticker_name):
        return self.places[ticker_name]