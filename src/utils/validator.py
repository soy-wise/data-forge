import datetime

class DataValidator:

    @staticmethod
    def _is_date_valid(date: str) -> bool:

        try:
            datetime.date.fromisoformat(date)
            return True
        except ValueError:
            return False