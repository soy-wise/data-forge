from src.extract.finance_data_extractor import FinanceDataExtractor

import pytest

class TestFinanceDataExtractor:

    data_extractor = FinanceDataExtractor('SOJA3.SA')
        

    def test_get_ticker_info(self):
        # ask for ticket info and validate the response
        ticker_info = self.data_extractor.get_ticker_info()
        assert ticker_info['symbol'] == 'SOJA3.SA'

    def test_get_ticker_data(self):

        df = self.data_extractor.get_ticker_data()

        # get column where date is 2021-04-30
        day = df[df.index == '2021-04-30']

        # validate the values for the day
        assert day['Open'][0] == pytest.approx(15.10)
        assert day['High'][0] == pytest.approx(17.88)
        assert day['Low'][0] == pytest.approx(15.00)
        assert day['Close'][0] == pytest.approx(16.20)
        assert day['Adj Close'][0] == pytest.approx(15.85649)
        assert day['Volume'][0] == 7654100
