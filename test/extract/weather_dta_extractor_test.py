from src.extract.weather_data_extractor import WeatherDataExtractor

import pandas as pd
import pytest


class TestWeatherDataExtractor:

    data_extractor = WeatherDataExtractor()


    def test_get_weather(self):

        # get the weather for the ticker SOJA3.SA
        ticker_name = 'SOJA3.SA'

        soja3_places = [
            'Formosa', 'Buritis', 
            'Cabeceiras', 'Jaborandi', 
            'Primavera do Leste', 'Uberlandia'
        ]

        df_columns = [
            'tavg', 'tmin',
            'tmax', 'prcp',
            'snow', 'wdir',
            'wspd', 'wpgt',
            'pres', 'tsun'
        ]

        weather = self.data_extractor.get_weather(ticker_name)

        for city in soja3_places:
            assert city in weather.keys()
            assert isinstance(weather[city], pd.DataFrame)
            assert list(weather[city].columns) == df_columns
  
